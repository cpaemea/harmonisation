
SELECT to_char(tableevent.eventkey),
       TABLEEVENT.EVENTCODE,
       TABLEEVENT.EVENTDESCRIPTION,
       to_char((SELECT COUNT (*)
          FROM caseevent
         WHERE CASEEVENT.EVENTKEY = TABLEEVENT.eventkey))
          AS nbEventInBdd,
        to_char((SELECT COUNT (*)
            FROM caseevent
           WHERE     CASEEVENT.EVENTKEY = TABLEEVENT.eventkey
                 AND CASEEVENT.CASEKEY IN ( SELECT casekey from PTA_LIVE) )) as nbEventInPTA,
        to_char((SELECT COUNT (*)
            FROM caseevent
           WHERE     CASEEVENT.EVENTKEY = TABLEEVENT.eventkey
                 AND CASEEVENT.CASEKEY IN ( SELECT casekey from PTF_LIVE ) )) as nbEventInPTF,
        TABLEEVENT.ISMAINEVENT as isMainEvent,

TABLEEVENT.*
 from tableevent
 order by TABLEEVENT.EVENTKEY 
 ;
 



SELECT to_char(tableevent.eventkey),
       TABLEEVENT.EVENTCODE,
       TABLEEVENT.EVENTDESCRIPTION,
       to_char((SELECT COUNT (*)
          FROM caseevent
         WHERE CASEEVENT.EVENTKEY = TABLEEVENT.eventkey))
          AS nbEventInBdd,
        to_char((SELECT COUNT (*)
            FROM caseevent
           WHERE     CASEEVENT.EVENTKEY = TABLEEVENT.eventkey
                 AND CASEEVENT.CASEKEY IN ( SELECT casekey from PTA_LIVE) )) as nbEventInPTA,
        to_char((SELECT COUNT (*)
            FROM caseevent
           WHERE     CASEEVENT.EVENTKEY = TABLEEVENT.eventkey
                 AND CASEEVENT.CASEKEY IN ( SELECT casekey from PTF_LIVE ) )) as nbEventInPTF,
        TABLEEVENT.ISMAINEVENT as isMainEvent,
        TABLESTATUS.STATUSCODE,
        TABLESTATUS.STATUSDESCRIPTION,
 (select listagg(VPNAME.name,'; ') within group (order by 1) from GlobalRelation GR
   inner join name VPNAME on VPNAME.NAMEKEY  = GR.RelatedKey
   where GR.RelationShipKey = 1200 and GR.RelatedTableName = 'NAME' and GR.ParentTableName = 'TABLEEVENT' and GR.ParentKey = TableEvent.EventKey) as "Filter by site",
   (select listagg(VPT.ValidPropertyTypeDescription,'; ') within group (order by 1) from GlobalRelation GR
   inner join ValidPropertyType VPT on VPT.ValidPropertyTypeKey = GR.RelatedKey
   where  VPT.VALIDPROPERTYTYPEKEY in (11,21) and GR.RelationShipKey = 1200 and GR.RelatedTableName = 'VALIDPROPERTYTYPE' and GR.ParentTableName = 'TABLEEVENT' and GR.ParentKey = TableEvent.EventKey) as "VALID FOR",
   (
select listagg(Tlaw.lawkey,'; ') within group (order by 1) from Tablelaw Tlaw where TLAW.EVENTKEY = TableEvent.EventKey and rownum < 10 group by TableEvent.EventKey   
) as "TableLawKey"       
 from tableevent
 left join tablestatus on TABLESTATUS.STATUSKEY = TABLEEVENT.PATENTSTATUSKEY
  order by TABLEEVENT.EVENTKEY 
 ;





select TABLEEVENT.EVENTCODE,
TABLEEVENT.EVENTDESCRIPTION,
TABLEEVENT.PATENTSTATUSKEY,
TABLESTATUS.STATUSCODE,
TABLESTATUS.STATUSDESCRIPTION
--VALIDFILINGTYPE.*,
--TABLELAW.* 
 from tableevent 
 left join tablestatus on TABLESTATUS.STATUSKEY = TABLEEVENT.PATENTSTATUSKEY
 LEFT JOIN TABLELAW ON TABLELAW.EVENTKEY = TABLEEVENT.EVENTKEY
LEFT JOIN ValidFilingType ON ValidFilingType.FilingTypeKey = TableLaw.FilingTypeKey and VALIDFILINGTYPE.PROPERTYTYPEKEY = 1    
 where TABLEEVENT.ISMAINEVENT = '1'   and VALIDFILINGTYPE.PROPERTYTYPEKEY = 1
 group by TABLEEVENT.EVENTCODE,TABLEEVENT.EVENTDESCRIPTION,TABLEEVENT.PATENTSTATUSKEY,TABLESTATUS.STATUSCODE,TABLESTATUS.STATUSDESCRIPTION  ; 