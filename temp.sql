
SELECT to_char(tableevent.eventkey) as eventkey,
       TABLEEVENT.EVENTCODE,
       TABLEEVENT.EVENTDESCRIPTION,
       count(caseve.caseeventkey) as nbEventInBdd,
--       count(distinct(caseve.caseeventkey)) 
       count(pta.casekey) as nbEventInPTA,
       count(ptf.casekey) as nbEventInPTF,
       count(distinct(pta.casekey)) as nbEventDistinctPTA,
       count(distinct(ptf.casekey)) as nbEventDistinctPTF
 from 
 tableevent
 left  join caseevent caseve on TABLEEVENT.EVENTKEY = caseve.EVENTKEY 
 left  join cases pta on pta.casekey = caseve.CASEKEY and pta.PROPERTYTYPEKEY = 1 and pta.CASETYPEKEY = 1
 left outer join cases ptf on ptf.casekey = caseve.CASEKEY and ptf.PROPERTYTYPEKEY = 1 and ptf.CASETYPEKEY = 2
group by tableevent.eventkey, TABLEEVENT.EVENTCODE, TABLEEVENT.EVENTDESCRIPTION
 ;
 
 -- >SRIP is null 20383794067 !!!
 --select * from tableevent ;
-- 
--select * from caseevent where 
 