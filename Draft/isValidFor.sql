--Valid For

select 
 TableEvent.EventKey,

TableEvent.EventCode, 

TableEvent.EventDescription,

(select listagg(VPT.ValidPropertyTypeDescription,'; ') within group (order by 1) from GlobalRelation GR

   inner join ValidPropertyType VPT on VPT.ValidPropertyTypeKey = GR.RelatedKey

   where GR.RelationShipKey = 1200 and GR.RelatedTableName = 'VALIDPROPERTYTYPE' and GR.ParentTableName = 'TABLEEVENT' and GR.ParentKey = TableEvent.EventKey) as "VALID FOR"       

from TableEvent

order by 1
